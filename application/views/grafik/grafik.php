<link href="<?php echo base_url();?>asset/adminn/datatable/datatable/bootstrap.min.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>asset/adminn/css/home.css">
 <script type="text/javascript" src="<?php echo base_url();?>asset/js/jquery.min.js"></script>
<link type="text/css" href="<?php echo base_url();?>asset/plugins/countdown/jquery.countdown.css?v=1.0.0.0" rel="stylesheet">
<script type="text/javascript" src="<?php echo base_url();?>asset/plugins/countdown/jquery.countdown.min.js?v=1.0.0.0"></script>

	<script type="text/javascript">
	function diagram(jk){
		if(jk == "Laki-Laki"){
			jk = 1;
		}else {
			jk = 2;
		}
		location.href="<?php echo base_url();?>index.php/admin/batang/"+jk;
	}
window.onload = function () {
	var chart = new CanvasJS.Chart("chartContainer",
	{
		theme: "theme2",
		title:{
			text: "Jumlah Jenis Kelamin Pemilih Pada Pemilu"
		},
		animationEnabled: true,
		animationDuration: 2000,
		legend: {
			verticalAlign: "bottom",
			horizontalAlign: "center"
		},

		data: [
		{
			click: function(e){
				//alert (e.dataSeries.type+e.dataPoint.y+e.dataPoint.indexLabel);
				diagram(e.dataPoint.indexLabel);
			},
			indexLabelFontSize: 20,
			indexLabelFontFamily: "Monospace",
			indexLabelFontColor: "darkgrey",
			indexLabelLineColor: "darkgrey",
			indexLabelPlacement: "outside",			
			type: "pie",
			showInLegend: true,
			toolTipContent: "{y} - #percent %",
			yValueFormatString: "##,,. Orang",
			legendText: "{indexLabel}",
			dataPoints: [
				{  y: <?php print_r ($jk[0]['pr']."000000");?>, indexLabel: "Perempuan" },
				{  y: <?php print_r ($jk[0]['laki']."000000");?>, indexLabel: "Laki-Laki"}	
			]
		}
		]
	});
	chart.render();
}
	</script>
	<script src="<?php echo base_url();?>asset/plugins/chart/piechart/canvasjs.min.js"></script>

<div id="wrapper">
	<div id="wrapper_menu">
		<div id="judulnya">Grafik</div>
		<div id="scrollnya">
		
		<div style="height:100%;"id="wrapper_submenu">
			<div id="juduldalem">
				<div id="chartContainer" style="padding-top:2vw;height: 400px; width: 95%;"></div>
				<div id="jumlaha"></div>
			</div>
			
		</div>
		</div>
	</div>
</div>