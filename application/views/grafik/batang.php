	    <link href="<?php echo base_url();?>asset/css/bootstrap.min.css" rel="stylesheet">
<?php
	if($kel == 1){
		$kela = "Laki - Laki";
	}else {
		$kela = "Perempuan";
	}
?>
	<script type="text/javascript">
		function datatabel(jk,prodi){
			location.href="<?php echo base_url();?>index.php/admin/tabel/"+jk+"/"+prodi;
		}
	
		window.onload = function () {
			var chart = new CanvasJS.Chart("chartContainer",
			{
				title: {
					text: "Program Studi Fakultas Sains dan Teknologi (<?php echo $kela;?>)"
				},
				animationEnabled: true,
				animationDuration: 2000,
				legend: {
					verticalAlign: "bottom",
					horizontalAlign: "center"
				},

				axisY: {
					stripLines: [{
						value: 143650,
						label: "Average",
						showOnTop: true
					}
					]
				},

				data: [
				{
					click: function(e){
						//alert (e.dataSeries.type+e.dataPoint.a+e.dataPoint.label);
						datatabel(e.dataPoint.a,e.dataPoint.label);
					},

					indexLabelFontSize: 20,
					indexLabelFontFamily: "Monospace",
					indexLabelFontColor: "darkgrey",
					indexLabelLineColor: "darkgrey",
					indexLabelPlacement: "outside",			
					type: "bar",
					dataPoints: [
						<?php $i = 10;
						foreach($prodi as $hasil_prodi){ ?>
						{ x: <?php echo $i;?>, y: <?php print_r ($hasil_prodi['hasil']);?>, a: "<?php echo $kel;?>", label: "<?php print_r ($hasil_prodi['prodi']);?>" },
						<?php
						$i += 10;
						}
						?>
						//{ x: 30, y: 8, a: "<?php echo $kel;?>", label: "Matematika" },
					]
				}
				]
			});

			chart.render();
		}
	</script>
	<script src="<?php echo base_url();?>asset/plugins/chart/piechart/canvasjs.min.js"></script>

<div id="wrapper">
	<div id="wrapper_menu">
		<div id="judulnya">Grafik</div>
		<div id="scrollnya">
		
		<div style="height:100%;"id="wrapper_submenu">
			<div id="juduldalem">
				<div id="chartContainer" style="height: 400px; width: 100%;"></div>
			</div>
				<div id="jumlaha">
	                <a class="btn btn-default submit" href="<?php echo base_url();?>index.php/admin/grafik">Kembali</a>
				</div>
		
		</div>
		</div>
	</div>
</div>