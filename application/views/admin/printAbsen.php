<?php
header("Content-type: application/vnd-ms-excel");
header("Content-Disposition: attachment; filename=Absen.xls");
?>


<!DOCTYPE html>
<html lang="en">

  <body class="nav-md">
	
	<div class="x_panel">
		<div class="fotoCalon">
			
  		            <div class="table-responsive">
                      <table class="table table-striped e bulk_action">
                        <thead>
                          <tr class="headings" id="hasil">
                            <th class="column-title">No</th>
                            <th class="column-title">NIM</th>
                            <th class="column-title">Nama</th>
                            <th class="column-title">Jenis Kelamin</th>
                            <th class="column-title">Program Studi</th>
                            <th class="column-title">Fakultas</th>
                          </tr>
                        </thead>

                        <tbody>
						<?php $i = 1;
						foreach($pemilih as $hpemilih){ ?>
						  <tr class="even pointer" id="hasil">
                            <td class=" "><?php echo $i;?></td>
                            <td class=" "><?php print_r ($hpemilih['nim']);?></td>
                            <td class=" "><?php print_r ($hpemilih['nama']);?></td>
                            <td class=" "><?php 
							if($hpemilih['jk'] == "P"){
								echo "Perempuan";
							}else {
								echo "Laki - Laki";
							}
							
							?></td>
                            <td class=" "><?php print_r ($hpemilih['namaunit']);?></td>
                            <td class=" "><?php print_r ($hpemilih['fak']);?></td>
                          </tr>						
						<?php
						$i++;
						}?>
                        </tbody>
                      </table>
                    </div>
                 </div>
              </div>
            </div>
        
      </div>
      </div>
    </div>


  </body>
</html>