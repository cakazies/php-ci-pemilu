

<div class="content-wrapper">
<section class="content">
<div id="wrapper">
	<section class="content-header">
      <h1 style="float:left;">
        Form Tambah Calon
      </h1>
	  		<?php
			$jum = $jumlah[0]['a'];
			if($jum == 0 || $jum == null){?>
			<input class="btn btn-success" style="margin-left:2vh;"  type="submit" name="simpan" onclick="lihat();" data-toggle="tab" href="#tambah" value="Tambah Calon"/>

		<?php }else { 
		?>
			<input class="btn btn-success" style="margin-left:2vh; float:left;" type="submit"  data-toggle="tab" onclick="lihat_f();" href=".tempatFot" value="Calon"/>
			<input class="btn btn-success" style="margin-left:2vh; float:left;"  type="submit" name="simpan" onclick="lihat();" data-toggle="tab" href="#tambah" value="Tambah Calon"/>
			<input  style="margin-left:2vh" class="btn btn-warning" type="submit" name="simpan" id="simpan" data-toggle="tab" href="#edit_calon" onclick="edit_calon();" value="Edit Calon"/>
		<?php
		}
		?>
     <ol class="breadcrumb">
        <li><a><i class="fa fa-plus-circle"></i> Tambah Calon</a></li>
       </ol>
    </section>
	
    <section class="content">
		<div class="row" style="">
			<div class="col-xs-12 col-sm-12 col-lg-12">
			<div class="box">
				<div class="box-body"style="">
					<div class="col-xs-12 col-sm-12 col-lg-12" style="">
						<div id="allFot" class="tab-pane fade in active">
						<?php $i=1; foreach($calon as $hasil_calon){ ?>
						<div class="col-xs-12 col-sm-6 col-lg-3" style="padding-top:2vh;">
							<div class="tempatFot">
									<img style="height:100%;width:100%;"  src="<?php echo base_url();?>asset/foto/<?php print_r ($hasil_calon['foto']); ?>"/><h3>Calon ke <?php echo $i;?></h3>
							</div>
						</div>
						<?php
						$i++;
						}
						if($jum == 0){ ?>
						<div class="tempatFot">
							<img style="height:100%;width:100%;"  src="<?php echo base_url();?>asset/foto/Pemilu.jpg"/>
						</div>
						<?php } ?>
						
						</div>
					</div>
					<div id="tambah" class="tab-pane fade">
						<form action="<?php echo site_url();?>index.php/admin/tambah_calon"  method="POST" enctype="multipart/form-data" >
							<div class="col-xs-12 col-sm-12 col-lg-12" style="padding-top:6vh;">
								<div class="col-xs-12 col-sm-12 col-lg-12">
									<div class="form-group" style="margin-left:-2vh;">
										<div class="col-xs-4 col-sm-6 col-lg-9">
											<input id="userfile" class="form-control col-md-12 col-xs-12" type="file" name="userfile"/>
										</div>
										<div class="col-xs-8 col-sm-6 col-lg-3">			
										<input   class="btn btn-success" type="submit" name="simpan" id="simpan" style="float:left;" value="Tambah"/>
										<input  style="margin-left:2vh " class="btn btn-default" type="submit" onclick="kembali();" value="Kembali"/>
										</div>
										<span style="font-size:15px;margin-left:2.2vh; color:#008d4c;">Max 1000kb</span>
									</div>
								</div>	  
							</div>
						</form>
					</div>
					<div id="edit_calon" class="tab-pane fade">
					
						<?php $i = 1;
						foreach($calon as $hasil_calon){ ?>
						<div class="row" style="padding-top:3vh">
						<div class="gambar col-xs-12 col-sm-4 col-lg-4" >
							<div id="fot" ><img style="height:100%;width:100%;"  src="<?php echo base_url();?>asset/foto/<?php print_r ($hasil_calon['foto']); ?>"/></div>
						</div>
						<form action="<?php echo site_url();?>index.php/admin/tambah_calon"  method="POST" enctype="multipart/form-data" >
							<div class="col-xs-12 col-sm-8 col-lg-8" style="padding-top:6vh;">
							<span style="font-size:4vh; font-weight:bold;margin-left:2vh; color:#003d1e;">Calon <?php echo $i; ?></span>
								<div class="col-xs-12 col-sm-12 col-lg-12">
									<div class="form-group" style="margin-left:-2vh;">
										<div class="col-xs-8 col-sm-9 col-lg-9">
											<input id="userfile" class="form-control col-md-12 col-xs-12" type="file" name="userfile"/>
										</div>
										<div class="col-xs-4 col-sm-3 col-lg-3">				
											<input  style="margin-left:2vh; " class="btn btn-warning" type="submit" name="simpan" id="simpan" value="Update"/>
										</div>
										<span style="font-size:15px;margin-left:2.2vh; color:#008d4c;">Max 1000kb</span>
									</div>
								</div>	  
							</div>
						</form>
						<br/>
						</div>
						<?php
						$i++;
						}
						?>
					</div>	
				</div>
			</div>	
			</div>
		</div>
	</section>
</div>
</section>
</div>

<footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>SINF</b> 2014
    </div>
    <strong>Copyright &copy; 2016-2017 SAINTEK</strong> 
</footer>

<script>
$(document).ready(function(){
$('.tempatFot').show();
$('#tambah').hide();
$('#edit_calon').hide();
});
function lihat_f() {
$('.tempatFot').show();
$('#tambah').hide();
$('#edit_calon').hide();
}
function lihat() {
$('.tempatFot').hide();
$('#tambah').show();
$('#edit_calon').hide();
}
function edit_calon() {
$('#edit_calon').show();
$('.tempatFot').hide();
$('#tambah').hide();
}
</script>

