<div class="content-wrapper">
<section class="content">
<div id="wrapper">
	<section class="content-header">
      <h1>
        Form Hapus
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo site_url();?>admin/index/home"><i class="fa fa-trash-o"></i>Hapus</a></li>
       </ol>
    </section>
	
    <section class="content">
		<div class="row" style="">
			<div class="col-xs-12 col-sm-12 col-lg-12">
				<div class="box">
					<div class="box-body"style="padding:4vh;">
						<form action="<?php echo site_url();?>index.php/admin/hapus_all"  method="POST" >
						  <div class="form-group" >
							  <label class="col-md-12 col-sm-12 col-xs-12" style="font-size:23px; margin-left:2vh; color:#003d1e;">Masukkan Password Untuk Menghapus Semua</label>
							  <input style="width:50%; margin-left:2vh;"  id="ask" class="form-control col-md-7 col-xs-12" type="password" name="ask"/>
							  <input  style="margin-left:3vh; " class="btn btn-success" type="submit" name="simpan" id="simpan" value="Submit"/>
						  </div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>
</section>
</div>
<footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>SINF</b> 2014
    </div>
    <strong>Copyright &copy; 2016-2017 SAINTEK</strong> 
</footer>
