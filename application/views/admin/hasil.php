<div class="content-wrapper">
<section class="content">
<div id="wrapper">
	<section class="content-header">
      <h1 style="font-size:6vh">
        Hasil Pemilihan Umum
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo site_url();?>admin/index/home"><i class="fa fa-bullhorn"></i> Lihat Hasil</a></li>
       </ol>
    </section>
	
    <section class="content">
		<div class="row" style="">
		<div class="col-md-4 col-sm-6 col-xs-12">
          <div class="info-box bg-green">
            <span class="info-box-icon"><i class="fa fa-users" style="padding-top:3vh;"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Total Mahasiswa</span>
              <span class="info-box-number"><?php print_r ($pemilih[0]['total']);?> mhs</span>

              <div class="progress">
                <div class="progress-bar" style="width: 100%"></div>
              </div>
                  <span class="progress-description">
                    100% <?php print_r ($kop3);?>
                  </span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
		<div class="col-md-4 col-sm-6 col-xs-12">
          <div class="info-box bg-yellow">
            <span class="info-box-icon"><i class="fa fa-user" style="padding-top:3vh;"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Mahasiswa Sudah Memilih</span>
              <span class="info-box-number" id="sudah_mhs"><?php print_r ($pemilih[0]['sudah']);?> mhs</span>

              <div class="progress">
                <div class="progress-bar" style="width: <?php 
						  $hasil2 = $pemilih[0]['sudah'] / $pemilih[0]['total'];
						  $persen = $hasil2 *100;
						  echo number_format($persen,2)."%";
						  ?>"></div>
              </div>
                  <span class="progress-description" id="pers_sudah">
                    <?php 
						  $hasil2 = $pemilih[0]['sudah'] / $pemilih[0]['total'];
						  $persen = $hasil2 *100;
						  echo number_format($persen,2)."%";
						  ?> Sudah Memilih
                  </span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
		<div class="col-md-4 col-sm-6 col-xs-12">
          <div class="info-box bg-red">
            <span class="info-box-icon"><i class="fa fa-user" style="padding-top:3vh;"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Mahasiswa Belum Memilih</span>
              <span class="info-box-number" id="belum_mhs"><?php print_r ($pemilih[0]['belum']);?> mhs</span>

              <div class="progress">
                <div class="progress-bar" style="width: <?php 
						  $hasil2 = $pemilih[0]['belum'] / $pemilih[0]['total'];
						  $persen = $hasil2 *100;
						  echo number_format($persen,2)."%";
						  ?>"></div>
              </div>
                  <span class="progress-description" id="pers_belum">
                    <?php 
						  $hasil2 = $pemilih[0]['belum'] / $pemilih[0]['total'];
						  $persen = $hasil2 *100;
						  echo number_format($persen,2)."%";
						  ?> Belum Memilih
                  </span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
			<div class="col-md-12 col-sm-12 col-xs-12" >
				<div class="box">
					<div class="box-body"style="padding:4vh;">
						<div class="col-md-6 col-sm-6 col-xs-12" style="min-height:60vh;">
								<div class="control-label " id="pesan" align="center">
									<a  style=" " href="javascript:cek_jam(<?php print_r ($jumlah[0]['a']);?>);">
										<img style="height:50vh;width:68vh;" id="gambar_kotak" src="<?php echo base_url();?>asset/images/as.gif"/>
									</a>
									<div class="ket" style="margin-top:-10vh; color:#008d4c">Klik Amplop untuk Melihat Hasil</div>
									<div id="jumlah" style="font-size:4vh;margin-top:-36vh;margin-left:30vh;"><?php print_r ($hasil[0]['a']);?></div>
								</div>
								<div id="timer" align="center" style="color:red;font-size:20vh;padding-top:10vh;"></div> </br>
								<span id="link"></span> 
						</div>
						<div class="col-md-6 col-sm-6 col-xs-12">
						  <div class="box box-success box-solid"style="margin-top:3vh;">
							<div class="box-header with-border">
							  <h1 class="box-title">Waktu Pemilu</h1>

							  <div class="box-tools pull-right">
								<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
								</button>
							  </div>
							  <!-- /.box-tools -->
							</div>
							<!-- /.box-header -->
							<div class="box-body">
							  <div id="countdon">
									<ul id="example" style="color:003d1e">
				<!-- 					  <li><span class="days">00</span><p class="days_text">Days</p></li>
										<li class="seperator">:</li>
				 -->					<li><span class="hours">00</span><p class="hours_text"style="color:003d1e">Hours</p></li>
										<li class="seperator">:</li>
										<li><span class="minutes">00</span><p class="minutes_text"style="color:003d1e">Minutes</p></li>
										<li class="seperator">:</li>
										<li><span class="seconds">00</span><p class="seconds_text"style="color:003d1e">Seconds</p></li>
									</ul>
								</div>
							</div>
							<!-- /.box-body -->
						  </div>
						  <!-- /.box -->
						</div>
					</div>
				</div>
			</div>
		</div>
</section>
</div>
</section>
</div>
<footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>SINF</b> 2014
    </div>
    <strong>Copyright &copy; 2016-2017 SAINTEK</strong> 
</footer>

<button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Buka Modal</button>
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
	<div class="modal-content">
	  <div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		<h4 class="modal-title" id="myModalLabel">Daftar Mahasiswa</h4>
	  </div>
	  <div class="modal-body" id="tabel_reset">
		
	  </div>
	  <div class="modal-footer">
		<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	  </div>
	</div>
  </div>
</div>

<link type="text/css" href="<?php echo base_url();?>asset/plugins/countdown/jquery.countdown.css?v=1.0.0.0" rel="stylesheet">
<script type="text/javascript" src="<?php echo base_url();?>asset/plugins/countdown/jquery.countdown.min.js?v=1.0.0.0"></script>
<script type="text/javascript" src="<?php echo base_url();?>asset/adminn/js/hasil.js"></script>
		
<div class="modal fade" id="modalPassword" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
	<div class="modal-content">
		<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		<h4 class="modal-title" id="myModalLabel">Modal title</h4>
		</div>
		<div class="modal-body">
			<form action="javascript:checkModal()" method="get">
				<input type="password" id="passwordInput" class="form-control">
			</form>
		</div>
		<div class="modal-footer">
		<a class="btn btn-primary" href="javascript:checkModal()">Sign Up</a>
		</div>
	</div>
	</div>
</div>
 
 <script type="text/javascript">
 
var counter = 3;
var hasil = 3;

function stik(as) {
	hasil = as;
}
function checkModal(){
	password = $('#passwordInput').val()
	if(password == "sapiderman") {
		$('#modalPassword').modal('hide');
		cek(3);
	}else {
		alert ("password salah")
	}
}
function cek_jam(as) {
	$('#modalPassword').modal('show');
	if(hasil == '7'){
	}else {
		// swal("Good job!", "You clicked the button!", "success")
		// swal("Maaf Waktu Pemilu Belum Selesai !");
		// swal({
		  // title: "Sweet!",
		  // text: "Here's a custom image.",
		  // imageUrl: "images/thumbs-up.jpg"
		// });
		// swal({
			// title: "Are you sure?",
			// text: "You will not be able to recover this imaginary file!",
			// type: "warning",
			// showCancelButton: true,
			// confirmButtonColor: "#DD6B55",
			// confirmButtonText: "Yes, delete it!",
			// closeOnConfirm: false
		// },
		// function(){
			// swal("Deleted!", "Your imaginary file has been deleted.", "success");
		// });
	}
}
function cek(as) {
	if(as < 2){
		swal({
		  title: "Jumlah Calon Minimal 2",
		  text: "Silahkan Periksa Jumlah Calon !!",
		  confirmButtonColor: "#008d4c",
		  confirmButtonText: "Oke",
		  closeOnConfirm: false,
		  closeOnCancel: false,
		  delay:60
		});
		window.location.assign("<?php echo base_url('index.php/admin/redirect') ?>");
	}else {
		countDown();
	}
}
function countDown() {
	$('#pesan').hide();
	$('#timer').show();

    if(counter>=0) {
	
        document.getElementById("timer").innerHTML = counter;
    }
    else {
        download();
        return;
    }
    counter -= 1;

    var counter2 = setTimeout("countDown()",1000);
    return;
}
function download() {
	//alert ("as");
    window.location = "<?php echo base_url()?>index.php/prin";
	//document.getElementById("link").innerHTML = "<a href='http://suckittrees.com'>Download</a>";
}

var i = setInterval(function(){

     $.ajax({
            url: "<?php echo base_url('index.php/admin/hasil') ?>",
            cache: false,
            success: function (html) {
				//alert (html);
				
                $("#jumlah").text(html);
            },
    });

	$.ajax({
            url: "<?php echo base_url('index.php/admin/sudah') ?>",
            cache: false,
            success: function (html) {
				//alert (html);
				
                $("#sudah_mhs").text(html);
            },
    });

	$.ajax({
            url: "<?php echo base_url('index.php/admin/belum') ?>",
            cache: false,
            success: function (html) {
				//alert (html);
				
                $("#belum_mhs").text(html);
            },
    });

	$.ajax({
            url: "<?php echo base_url('index.php/admin/pers_belum') ?>",
            cache: false,
            success: function (html) {
				//alert (html);				
                $("#pers_belum").text(html);
            },
    });
	
	$.ajax({
            url: "<?php echo base_url('index.php/admin/pers_sudah') ?>",
            cache: false,
            success: function (html) {
				//alert (html);
				
                $("#pers_sudah").text(html);
            },
    });

},6000)

</script>
	<script type="text/javascript">
		$('#example').countdown({
			date: '<?php print_r ($tahun[0]['hasil']." ".$jam[0]['isi']);?>:00:00',
			offset: -0,
			day: 'Day',
			days: 'Days'
		}, function () {
			// swal({
			// 	title: "DONE",
			// 	text: "Silahkan Klik gambar Amplop !!",
			// 	  confirmButtonColor: "#008d4c",
			// 	  confirmButtonText: "Oke",
			// 	  closeOnConfirm: false,
			// 	  closeOnCancel: false
			// });
			// stik(7);
		});
		
		
	</script>
 