<html >
	<head>
		<!-- <meta http-equiv="refresh" content="2"></meta> -->
		
		<title> Komisi Pemilihan Umum</title>
		<link href='<?php echo base_url();?>asset/images/KPU.png' rel='SHORTCUT ICON'/>
		<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>asset/adminn/css/reset.css">
		<!--<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>asset/adminn/css/style.css">
		<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>asset/adminn/css/dosen.css">-->
		<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>asset/adminn/css/new.css">
		<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>asset/css/vendor/font-awesome/css/font-awesome.min.css">
		<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>asset/css/bootstrap.min.css">
		
		  <!-- Ionicons -->
		  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
		  <!-- Theme style -->
		  <link rel="stylesheet" href="<?php echo base_url();?>asset/adminn/css/vendor/AdminLTE.min.css">
		  <!-- AdminLTE Skins. Choose a skin from the css/skins
			   folder instead of downloading all of them to reduce the load. -->
		  <link rel="stylesheet" href="<?php echo base_url();?>asset/adminn/css/vendor/_all-skins.min.css">
		  <!-- iCheck -->
		  <link rel="stylesheet" href="<?php echo base_url();?>asset/adminn/css/vendor/blue.css">
		  <!-- Morris chart -->
		  <link rel="stylesheet" href="<?php echo base_url();?>asset/adminn/css/vendor/morris.css">
		  <!-- jvectormap -->
		  <link rel="stylesheet" href="<?php echo base_url();?>asset/adminn/css/vendor/jquery-jvectormap-1.2.2.css">
		  <!-- Date Picker -->
		  <link rel="stylesheet" href="<?php echo base_url();?>asset/adminn/css/vendor/datepicker3.css">
		  <!-- Daterange picker -->
		  <link rel="stylesheet" href="<?php echo base_url();?>asset/adminn/css/vendor/daterangepicker.css">
		  
		<!-- Sweet Alert-->
		<link rel="stylesheet" href="<?php echo base_url();?>asset/css/dist/sweetalert.css">
		<script src="<?php echo base_url();?>asset/css/dist/sweetalert.min.js"></script>
		
		<script>
			function konf(){
				var pilih = confirm("Yakin Bang Sudah Bener Semua ??");
				if(pilih){
					var pilih = confirm("Monggo Di Cek Dulu ke dua kalinya ??");
					if(pilih){
						window.location.assign("<?php echo base_url('index.php/admin/konfirmasi') ?>")
					}
				}
			}
			function as(){
				var pilih = confirm("Yakin Dihapus..!");
				if(pilih){
					window.location.assign("<?php echo base_url('index.php/admin/index/hapus') ?>")
				}
			}
			function perbaruiMHS(){
				var pilih = confirm("Perbarui Mahasiswa ?");
				if(pilih){
					$.ajax({
							url: "<?php echo base_url('index.php/home/jajal') ?>",
							cache: false,
							success: function (html) {
								alert (html);
							},
					});
				}
			}
			$(document).ready(function(){
				$('#edit_calon').hide();
				$('#tambah_calon').hide();
				$('#timer').hide();
				$('#juduldalem').show();
	
				//Examples of how to assign the Colorbox event to elements
				//$(".group1").apik({rel:'grofgsdfup1'});
				
				
			});
			
		</script>
		
		<!-- JS LTE -->
		<!-- jQuery 2.2.3 -->
		<script src="<?php echo base_url();?>asset/adminn/js/vendor/jquery-2.2.3.min.js"></script>
		<!-- jQuery UI 1.11.4 -->
		<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
		<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
		<script>
		  $.widget.bridge('uibutton', $.ui.button);
		</script>
		<!-- Bootstrap 3.3.6 -->
		<script src="<?php echo base_url();?>asset/adminn/js/vendor/bootstrap.min.js"></script>
		<!-- Morris.js charts -->
		<script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
		<script src="<?php echo base_url();?>asset/adminn/js/vendor/morris.min.js"></script>
		<!-- Sparkline -->
		<script src="<?php echo base_url();?>asset/adminn/js/vendor/jquery.sparkline.min.js"></script>
		<!-- jvectormap -->
		<script src="<?php echo base_url();?>asset/adminn/js/vendor/jquery-jvectormap-1.2.2.min.js"></script>
		<script src="<?php echo base_url();?>asset/adminn/js/vendor/jquery-jvectormap-world-mill-en.js"></script>
		<!-- jQuery Knob Chart -->
		<script src="<?php echo base_url();?>asset/adminn/js/vendor/jquery.knob.js"></script>
		<!-- daterangepicker -->
		<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
		<script src="<?php echo base_url();?>asset/adminn/js/vendor/daterangepicker.js"></script>
		<!-- datepicker -->
		<script src="<?php echo base_url();?>asset/adminn/js/vendor/bootstrap-datepicker.js"></script>
		<!-- Bootstrap WYSIHTML5 -->
		<script src="<?php echo base_url();?>asset/adminn/js/vendor/bootstrap3-wysihtml5.all.min.js"></script>
		<!-- Slimscroll -->
		<script src="<?php echo base_url();?>asset/adminn/js/vendor/jquery.slimscroll.min.js"></script>
		<!-- FastClick -->
		<script src="<?php echo base_url();?>asset/adminn/js/vendor/fastclick.js"></script>
		<!-- AdminLTE App -->
		<script src="<?php echo base_url();?>asset/adminn/js/vendor/app.min.js"></script>
		<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
		<script src="<?php echo base_url();?>asset/adminn/js/vendor/dashboard.js"></script>
		<!-- AdminLTE for demo purposes -->
		<script src="<?php echo base_url();?>asset/adminn/js/vendor/demo.js"></script>
		<script src="<?php echo base_url();?>asset/js/proses.js"></script>
		<script src="<?php echo base_url();?>asset/js/jquery.ajaxQueue.min.js"></script>
		<script>
			function konf(){
				swal({
				  title: "Yakin Sudah Bener Semua ??",
				  text: "Silahkan Di Cek Kembali !!",
				  type: "warning",
				  showCancelButton: true,
				  confirmButtonColor: "#008d4c",
				  confirmButtonText: "Yakin",
				  cancelButtonText: "Batal",
				  closeOnConfirm: false,
				  closeOnCancel: true
				},
				function(isConfirm) {
				  if (isConfirm) {
					swal({
						title:"Data di Simpan", 
						text:"Semua data sudah tersimpan.", 
						type:"success",
						confirmButtonColor: "#008d4c",
						confirmButtonText: "Oke"});
					window.location.assign("<?php echo base_url('index.php/admin/konfirmasi') ?>")
				  }
				});
			}
			function as(){
				swal({
				  title: "Yakin Dihapus?",
				  text: "Pastikan Sekali Lagi!",
				  type: "warning",
				  showCancelButton: true,
				  confirmButtonClass: "btn-danger",
				  confirmButtonColor: "#008d4c",
				  confirmButtonText: "Hapus",
				  cancelButtonText: "Batal",
				  closeOnConfirm: false,
				  closeOnCancel: true
				},
				function(isConfirm) {
				  if (isConfirm) {
					window.location.assign("<?php echo base_url('index.php/admin/index/hapus') ?>")
				  } 
				});
			}
			
		</script>

		
	</head>
	<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <header class="main-header">
    <!-- Logo -->
    <a href="index2.html" class="logo">
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>KPU</b></span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
       
          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <img src="<?php echo base_url();?>asset/images/w.png" class="user-image" alt="User Image">
              <span class="hidden-xs">Admin</span>
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
                <img src="<?php echo base_url();?>asset/images/w.png" class="img-circle" alt="User Image">

                <p>
                  Mblendes Varokah - Web Developer
                  <small>SINF 2014</small>
                </p>
              </li>
              <!-- Menu Footer-->
              <li class="user-footer">
                <div class="pull-left">
                  <a href="#"  class="btn btn-default btn-flat">
					Edit
				  </a>
                </div>
                <div class="pull-right">
                  <a href="<?php echo site_url();?>index.php/admin/logout" class="btn btn-default btn-flat">Sign out</a>
                </div>
              </li>
            </ul>
          </li>
          <!-- Control Sidebar Toggle Button -->
          <li>
            <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
          </li>
        </ul>
      </div>
    </nav>
  </header>
  
  
    <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="<?php echo base_url();?>asset/images/kpu.png"  alt="User Image">
		</div>
        <div class="pull-left info">
          <p>Komisi </br>Pemilihan Umum</p>
        </div>
      </div>
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" style="margin-top:2vw;">
        <li class="header">Menu</li>
        <?php 
			if($hak[0]['isi'] == 0){ ?>
		<!-- <li class="treeview">
          <a href="<?php echo site_url();?>index.php/admin/index/dashboard">
            <i class="fa fa-dashboard"></i> 
			<span>Dashboard</span>
          </a>
        </li> -->
        <li class="treeview">
          <a href="<?php echo site_url();?>index.php/admin">
            <i class="fa fa-plus-circle"></i>
            <span>Tambah Calon</span>
          </a>
        </li>
		<?php 
		if($prodi == 2){ ?>
        <li class="treeview">
          <a href="<?php echo site_url();?>index.php/admin/partai">
            <i class="fa fa-plus-circle"></i>
            <span>Tambah Partai</span>
          </a>
        </li>
		
		<?php 
		}else { ?>
		
		<?php
		}?>
        <li class="treeview">
          <a href="javascript:konf();">
            <i class="fa fa-key"></i>
            <span>Kunci Calon</span>
          </a>
        </li>
        <li class="treeview">
          <a href="<?php echo site_url();?>index.php/admin/index/pengaturan">
            <i class="fa fa-cogs"></i>
            <span>Pengaturan</span>
          </a>
        </li>
        <li class="treeview">
          <a href="<?php echo site_url();?>index.php/home/jajal">
			<!-- <a href="javascript:perbaruiMHS();"> -->
            <i class="fa fa-cogs"></i>
            <span>Perbarui Mahasiswa</span>
          </a>
        </li>
		<li class="treeview">
          <a href="javascript:as();">
            <i class="fa fa-trash-o"></i>
            <span>Hapus</span>
          </a>
        </li>
		<?php  }else { ?>
        <li class="treeview">
          <a href="<?php echo site_url();?>index.php/admin/index/hasil">
            <i class="fa fa-bullhorn"></i>
            <span>Lihat Hasil</span>
          </a>
        </li>
<!-- 		<li class="treeview">
          <a href="javascript:as();">
            <i class="fa fa-trash-o"></i>
            <span>Hapus</span>
          </a>
        </li>
 -->
		<?php 
			}
			?>
        
    </section>
    <!-- /.sidebar -->
  </aside>
  
  
  
  