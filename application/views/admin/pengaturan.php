<div class="content-wrapper">
<section class="content">
<div id="wrapper">
	<section class="content-header">
      <h1>
        Pengaturan
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo site_url();?>admin/index/home"><i class="fa fa-cogs"></i>Pengaturan</a></li>
       </ol>
    </section>
	
    <section class="content">
		<div class="row" style="">
				<div class="box">
					<form  action="<?php echo site_url();?>index.php/admin/tetapfak" method="post">
					<div class="box-body"style="padding:4vh;">
					<div class="col-md-12 col-sm-12 col-xs-12">
						<div class="form-group">
							<div class="row">
								<label class="control-label col-md-3 col-sm-3 col-xs-12" style="">Fakultas</label>
								<select class="form-control col-md-9 col-sm-9 col-xs-12" style="width:70%" id="fakultas" name="fakultas" onclick="javascript:pilih_prodi();">
									<?php 
									foreach($fakultas as $hfakultas){ ?>
									<option value="<?php print_r ($hfakultas['kodeunit'])?>" <?php if($ttpfak[0]['fakultas'] == $hfakultas['kodeunit']){echo 'selected="selected"';}?>><?php print_r ($hfakultas['namaunit'])?></option>
										
									<?php
									}
									?>
								</select>  
							</div>
						</div>	
						<div class="form-group">
							<div class="row">
							  <label class="control-label col-md-3 col-sm-3 col-xs-12" style="">Program Studi</label>
								<select class="form-control col-md-9 col-sm-9 col-xs-12" style="width:70%" id="prodi" name="prodi">
									<option value="<?php print_r ($ttpfak[0]['prodi'])?>"><?php print_r ($ttpfak[0]['namaunit'])?></option>
									
								</select>    
							</div>	
						</div>	
					</div>
					</div>
					<div class="modal-footer">
						<input type="submit" class="btn btn-success"  value="Simpan" />
					</div>
					</form>
				</div>
				<div class="box">
					<form  action="<?php echo site_url();?>index.php/admin/tetapjam" method="post">
					<div class="box-body"style="padding:4vh;">
					<div class="col-md-12 col-sm-12 col-xs-12">
						<div class="form-group">
							<div class="row">
								<label class="control-label col-md-3 col-sm-3 col-xs-12" style="">Jam Berakhir</label>
								<select class="form-control col-md-9 col-sm-9 col-xs-12" style="width:70%" id="jam" name="jam" >
									<?php 
									$jam = $jam[0]['isi']+7;
									for($i=12;$i<=17;$i++){ ?>
									<option value="<?php echo $i-7; ?>" <?php if($jam == $i){echo 'selected="selected"';}?>><?php echo $i;?></option>										
									<?php
									}
									?>
								</select>  
							</div>
						</div>	
					</div>
					</div>
					<div class="modal-footer">
						<input type="submit" class="btn btn-success"  value="Simpan" />
					</div>
					</form>
				</div>
		</div>
	</section>
</div>
</section>
</div>
<footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>SINF</b> 2014
    </div>
    <strong>Copyright &copy; 2016-2017 SAINTEK</strong> 
</footer>
<script>
function pilih_prodi(){
	var fak = $("#fakultas").val();
	$("#prodi").load("<?php echo base_url();?>index.php/admin/prodi/"+fak)
}
</script>