<?php 
class admin_model extends CI_Model {
	function __construct(){
		parent::__construct();
	}
	function login($username,$password){
		$this->db->select('count(*) as a');
		$this->db->from('login');
		$this->db->where('username', $username);
		$this->db->where('password', $password);
		$query = $this->db->get();
		$data = $query->result_array();

 		if($data[0]['a'] == 0){
			//cocok
			return 0;
		} else {
			//tidak cocok 
			return 1;
		}
	}
	function cek_login($username,$password){
		$this->db->select('count(*) as a');
		$this->db->from('login');
		$this->db->where('username', $username);
		$this->db->where('password', $password);
		$query = $this->db->get();
		$data = $query->result_array();

 		if($data[0]['a'] == 0){
			//cocok
			return 0;
		} else {
			//tidak cocok 
			return 1;
		}
	}
	function cek_login_mhs($username,$password){
		$this->db->select('count(*) as a');
		$this->db->from('mahasiswa');
		$this->db->where('nim', $username);
		$this->db->where('password', $password);
		$query = $this->db->get();
		$data = $query->result_array();

 		if($data[0]['a'] == 0){
			//cocok
			return 0;
		} else {
			//tidak cocok 
			return 1;
		}
	}
	function cekMhsStatus($username){
		$this->db->select('status as a');
		$this->db->from('mahasiswa');
		$this->db->where('nim', $username);
		$query = $this->db->get();
		$data = $query->result_array();

		return $data[0]['a'];
	}
	function all_hapus(){
		$query = $this->db->get('calon');
		$data = $query->result_array();
		foreach($data as $hasil){			
			unlink("asset/foto/".$hasil['foto']);
		}
		$query = $this->db->get('partai');
		$data = $query->result_array();
		foreach($data as $hasil){			
			unlink("asset/foto/".$hasil['foto']);
		}
		
		$this->db->truncate('partai'); 
		$this->db->truncate('calon'); 
		$this->db->truncate('jum_pemilih'); 
	}
	function getAllCalon(){
		$this->db->select('count(*) as a');
		$this->db->from('calon');
		$query = $this->db->get();
		$data = $query->result_array();
		return $data;
	}
	function getNowFakultas(){
		$this->db->select('fakultas');
		$this->db->from('gate_ms_prodi2');
		$query = $this->db->get();
		$data = $query->result_array();
		return $data[0];
	}
	function getAllPartai(){
		$this->db->select('count(*) as a');
		$this->db->from('partai');
		$query = $this->db->get();
		$data = $query->result_array();
		return $data;
	}
	function getFakul(){
		$this->db->select('*');
		$this->db->from('gate_ms_unit');
		$query = $this->db->get();
		$data = $query->result_array();
		return $data;
	}
	function getTahun(){
		$strSql = "SELECT DATE_FORMAT(CURDATE(),'%m/%d/%Y') AS hasil;";
		$query = $this->db->query($strSql);
		$data = $query->result_array();
		return $data;
	}
	function getPemilih(){
		$strSql = "SELECT SUM(`status` = '2') as sudah, COUNT(*) as total , SUM(`status` = '1') as belum FROM mahasiswa;";
		$query = $this->db->query($strSql);
		$data = $query->result_array();
		return $data;
	}
	function getPemilih2($fakultas = "1",$prodi = '1'){
		if($fakultas == 1 ){
			//Dema Univ
			$strSql = "SELECT SUM(a.status = '2') AS sudah, COUNT(*) AS total , SUM(a.status = '1') AS belum  
						FROM (`mahasiswa` a) 
						JOIN `gate_ms_prodi` b ON `a`.`prodi` = `b`.`kodeunit` ;";
			$query = $this->db->query($strSql);
			$data = $query->result_array();
			return $data;
		}else if($prodi == 2){
			// dema fak
			$strSql = "SELECT SUM(a.status = '2') AS sudah, COUNT(*) AS total , SUM(a.status = '1') AS belum  
						FROM (`mahasiswa` a) JOIN `gate_ms_prodi` b ON `a`.`prodi` = `b`.`kodeunit` 
						JOIN `gate_ms_unit` d ON `d`.`kodeunit` = `b`.`kodeunitparent` 
						JOIN `gate_ms_prodi2` c ON `d`.`kodeunit`= `c`.`fakultas` ;";
			$query = $this->db->query($strSql);
			$data = $query->result_array();
			return $data;
			
		}else {
			//himaprodi
			$strSql = "SELECT SUM(a.status = '2') AS sudah, COUNT(*) AS total , SUM(a.status = '1') AS belum  
						FROM (`mahasiswa` a) JOIN `gate_ms_prodi` b ON `a`.`prodi` = `b`.`kodeunit` 
						JOIN `gate_ms_prodi2` c ON `a`.`prodi`= `c`.`prodi` ;";
			$query = $this->db->query($strSql);
			$data = $query->result_array();

			$strSql = "SELECT COUNT(*) AS sudah FROM `jum_pemilih`;";
			$query = $this->db->query($strSql);
			$datas = $query->result_array();
			$data[0]['sudah'] = $datas[0]['sudah'];
			$data[0]['belum'] = $data[0]['total']-$datas[0]['sudah'];
			return $data;
		}
	}
	function getPemilihAbsen(){		
			//Dema Univ
		$strSql = "SELECT a.nim , a.nama,a.jk , b.namaunit , c.namaunit as fak
					FROM (`mahasiswa` a) 
					JOIN `gate_ms_prodi` b ON `a`.`prodi` = `b`.`kodeunit`
					JOIN `gate_ms_unit` c ON `c`.`kodeunit` = `b`.`kodeunitparent` 
					WHERE a.status = 2;";
		$query = $this->db->query($strSql);
		$data = $query->result_array();
		return $data;
	}

	function getHasilMHS($query){
		$strSql = "SELECT $query FROM mahasiswa;";
		$query = $this->db->query($strSql);
		$data = $query->result_array();
		return $data;
	}
	function getJK(){
		$strSql = "SELECT SUM(jk = 'L') AS laki,SUM(jk = 'P') AS pr FROM jum_pemilih ;";
		$query = $this->db->query($strSql);
		$data = $query->result_array();
		return $data;
	}
	function indertMHS($as){
		$strSql = $as;
		$query = $this->db->query($strSql);
	}
	function getProdi($id){
		$strSql = "SELECT prodi, SUM(jk = '$id') as hasil FROM mahasiswa 
					WHERE `status` = '2'
					GROUP BY prodi;";
		$query = $this->db->query($strSql);
		$data = $query->result_array();
		return $data;
	}
	function simpan($table,$data){
		$this->db->insert($table,$data);
		$param = $this->db->affected_rows();
		return $param;
	}
	function getCalon(){
		$query = $this->db->get('calon');
		$data = $query->result_array();
		return $data;
	}
	function getPartai(){
		$query = $this->db->get('partai');
		$data = $query->result_array();
		return $data;
	}
	function getAllDashboard($id = '1'){
		$query = $this->db->get_where('dashboard', array('id' => $id));
		$data = $query->result_array();
		return $data;
	}
	function getAllhasil(){
		$this->db->select('count(*) as a');
		$this->db->from('jum_pemilih');
		$query = $this->db->get();
		$data = $query->result_array();
		return $data;
	}
	function simpan_calon($foto){
		$data = array(
		   'nama' => '..' ,
		   'foto' => $foto
		);
		$this->db->insert('calon', $data);
	}
	function simpan_partai($foto){
		$data = array(
		   'nama' => '..' ,
		   'foto' => $foto
		);
		$this->db->insert('partai', $data);
	}
	function tambah($id = "",$nim,$jk ,$prodi,$very){
		$data = array(
		   'id_calon' => $id ,
		   'nim' => $nim,
		   'jk' => $jk,
		   'status' => '1',
		   'prodi' => $prodi,
		   'very' => $very
		);
		$this->db->insert('jum_pemilih', $data);
	}
	function tambah2($id = "",$nim,$jk ,$prodi,$very){
		$data = array(
		   'id_calon' => $id ,
		   'nim' => $nim,
		   'jk' => $jk,
		   'status' => '1',
		   'prodi' => $prodi,
		   'very' => $very
		);
		$this->db->insert('pemilih', $data);
	}
	function tambah_partai($id = "",$nim,$jk ,$prodi){
		$data = array(
		   'id_partai' => $id ,
		   'nim' => $nim,
		   'jk' => $jk,
		   'status' => '1',
		   'prodi' => $prodi
		);
		$this->db->insert('partai_pemilih', $data);
	}
	function getTable($jk,$prodi){
		$this->db->select('*');
		$this->db->from('mahasiswa');
		$this->db->where('status', '2');
		$this->db->where('prodi', $prodi);
		$this->db->where('jk', $jk);
		$query = $this->db->get();
		$data = $query->result_array();
		return $data;
	}
	function getProdiFak($fak){
		$this->db->select('*');
		$this->db->from('gate_ms_prodi');
		$this->db->where('kodeunitparent', $fak);
		
		$query = $this->db->get();
		$data = $query->result_array();
		//print_r ($data);
		if($fak == 1){
			echo "<option value='2'>All</option>";
		}else {
			echo "<option value='2'>All</option>";
			foreach($data as $hdata){
				print_r ("<option value='".$hdata['kodeunit']."'>".$hdata['namaunit']."</option>");
			}
		}
	}
	function update_calon($foto,$id){
		//echo $id;
		$this->db->select('*');
		$this->db->from('calon');
		$this->db->where('id', $id);
		$query = $this->db->get();
		$data = $query->result_array();
		foreach($data as $hasil){			
			unlink("asset/foto/".$hasil['foto']);
		}
		$data = array(
		   'foto' => $foto
		);
		$this->db->where('id', $id);
		$this->db->update('calon', $data); 
	}
	function update_partai($foto,$id){
		//echo $id;
		$this->db->select('*');
		$this->db->from('partai');
		$this->db->where('id', $id);
		$query = $this->db->get();
		$data = $query->result_array();
		foreach($data as $hasil){			
			unlink("asset/foto/".$hasil['foto']);
		}
		$data = array(
		   'foto' => $foto
		);
		$this->db->where('id', $id);
		$this->db->update('partai', $data); 
	}
	function cek_absen($nim,$pass){
		$this->db->select('count(*) as hasil');
		$this->db->from('mahasiswa');
		$this->db->where('nim', $nim);
		$this->db->where('password', $pass);
		$query = $this->db->get();
		$data = $query->result_array();
		return $data;
	}
	function hapusDataMhs(){
		$this->db->truncate('mahasiswa'); 
	}
	function getJum(){
		$this->db->select('id_calon,nim,jk,status,prodi,very');
		$this->db->from('jum_pemilih');
		$query = $this->db->get();
		$data = $query->result_array();
		return $data;
	}
	function getidjum(){
		$this->db->select('id');
		$this->db->from('jum_pemilih');
		$query = $this->db->get();
		$data = $query->result_array();
		return $data;
	}
	function edit_judul($judul){
		$data = array(
		   'isi' => $judul
		);
		$this->db->where('id', '1');
		$this->db->update('dashboard', $data); 
	}
	function tetapfak($fakultas,$prodi){
		$data = array(
		   'fakultas' => $fakultas,
		   'prodi' => $prodi
		);
		$this->db->where('id', '1');
		$this->db->update('gate_ms_prodi2', $data); 
	}
	function update_password_mhs($nim,$data){
		$this->db->where('nim', "$nim");
		$this->db->update('mahasiswa', $data); 
	}
	function edit_konfirmasi($as='0'){
		$data = array(
		   'isi' => $as
		);
		$this->db->where('id', '2');
		$this->db->update('dashboard', $data); 
	}
	function tetapjam($jam){
		$data = array(
		   'isi' => $jam
		);
		$this->db->where('id', '3');
		$this->db->update('dashboard', $data); 
	}
	function update_absen($nim,$as='1'){
		$data = array(
		   'status' => $as
		);
		$this->db->where('nim', $nim);
		$this->db->update('mahasiswa', $data); 
	}
	function get_mhs($fakultas,$prodi){
		if($fakultas == 1 ){
			//Dema Univ
			$this->db->select('a.*,b.namaunit');
			$this->db->from('mahasiswa a');
			$this->db->join('gate_ms_prodi b', 'a.prodi = b.kodeunit');
			$this->db->where('a.status', '1');
			$query = $this->db->get();
			$data = $query->result_array();
			return $data;
		}else if($prodi == 2){
			// dema fak
			$this->db->select('a.*,b.namaunit');
			$this->db->from('mahasiswa a');
			$this->db->join('gate_ms_prodi b', 'a.prodi = b.kodeunit');
			$this->db->join('gate_ms_unit d', 'd.kodeunit = b.kodeunitparent');
			$this->db->join('gate_ms_prodi2 c', 'd.kodeunit= c.fakultas');
			$this->db->where('a.status', '1');
			$query = $this->db->get();
			$data = $query->result_array();
			return $data;
			
		}else {
			//himaprodi
			$this->db->select('a.*,b.namaunit');
			$this->db->from('mahasiswa a');
			$this->db->join('gate_ms_prodi b', 'a.prodi = b.kodeunit');
			$this->db->join('gate_ms_prodi2 c', 'a.prodi= c.prodi');
			$this->db->where('a.status', '1');
			$query = $this->db->get();
			$data = $query->result_array();
			return $data;
		}
	}
	function get_Kop(){
		$this->db->select('c.namaunit as fakultas, b.namaunit as prodi');
		$this->db->from('gate_ms_prodi2 a');
		$this->db->join('gate_ms_prodi b ', 'a.prodi =  b.kodeunit');
		$this->db->join('gate_ms_unit c', 'a.fakultas = c.kodeunit');
		$query = $this->db->get();
		$data = $query->result_array();
		return $data;
	}
	function get_ttpProdi(){
		$this->db->select('*');
		$this->db->from('gate_ms_prodi2');
		$this->db->where('id', '1');
		$query = $this->db->get();
		$data = $query->result_array();
		return $data;
	}
	function setting(){
		$this->db->select('status');
		$this->db->from('setting');
		$this->db->where('id', '1');
		$query = $this->db->get();
		$data = $query->result_array();
		return $data;
	}
	
	function getMhsAbsen($nim){
		$this->db->select('*');
		$this->db->from('mahasiswa');
		$this->db->where('nim', $nim);
		$query = $this->db->get();
		$data = $query->result_array();
		return $data;
	}
	function getTtpFakul(){
		$this->db->select('a.*,b.namaunit');
		$this->db->from('gate_ms_prodi2 a');
		$this->db->join('gate_ms_prodi b', 'a.prodi = b.kodeunit');
		$this->db->where('a.id', '1');
		$query = $this->db->get();
		$data = $query->result_array();
		return $data;
	}
	function duplicateAbsen($nim = "0"){
		$this->db->select('COUNT(*) as hasil');
		$this->db->from('jum_pemilih');
		$this->db->where('nim', $nim);
		$query = $this->db->get();
		$data = $query->result_array();
		return $data;
	}
	function duplicateAbsenPar($nim = "0"){
		$this->db->select('COUNT(*) as hasil');
		$this->db->from('partai_pemilih');
		$this->db->where('nim', $nim);
		$query = $this->db->get();
		$data = $query->result_array();
		return $data;
	}
	function nimError($nim = "0"){
		$this->db->delete("jum_pemilih", array('nim' => $nim)); 
		$this->db->delete("pemilih", array('nim' => md5($nim))); 
		$data = array(
		   'status' => '1'
		);
		$this->db->where('nim', $nim);
		$this->db->update('mahasiswa', $data); 
	}
	

	
}

?>