	<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Loading extends CI_Controller {
	function __construct(){
		parent::__construct();
		$this->load->database();
		$this->load->library('session');
		$this->load->model('Admin_model');
		$this->load->helper(array('form','url','file','download','text'));
		error_reporting(E_ALL ^ (E_NOTICE | E_WARNING));
		
	}

	
	public function index($admin = "loading"){
			$this->load->view('admin/loading');
	}
}
