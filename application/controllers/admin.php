	<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends CI_Controller {
	function __construct(){
		parent::__construct();
		$this->load->database();
		$this->load->library('session');
		$this->load->model('Admin_model');
		$this->load->helper(array('form','url','file','download','text'));
		// $this->load->helper('menu_helper');
		error_reporting(E_ALL ^ (E_NOTICE | E_WARNING));
		
	}
	public function index($admin = "home"){
		$jum_pem = $this->Admin_model->getJum();
		$cekdup = 0;
		$i=0;
		$dataMHS ;
		foreach($jum_pem as $hjum_pem){		
			$dupli = $hjum_pem['nim']."|".$hjum_pem['id_calon'];
			//$duplicade = base64_encode($dupli);
			$duplicade = md5($dupli);
			if($duplicade == $hjum_pem['very']){

			}else {
				
				$dataMHS[$i] = $hjum_pem['nim'];
				$cekdup = 1;
				$i++;
			}
		}
		if($cekdup == 1){
			//echo "gagal Mas ";
			//print_r ($dataMHS);
			$data['error'] = $dataMHS;
			$this->load->view('admin/eror',$data);
		}else {
			//echo "lanjutkan";
			//print_r ($jum_pem);
			$simpan = $this->session->userdata('username');
			$data['calon'] = $this->Admin_model->getCalon();
			$data['jumlah'] = $this->Admin_model->getAllCalon();
			$data['judul'] = $this->Admin_model->getAllDashboard(1);
			$data['hak'] = $this->Admin_model->getAllDashboard(2);
			$data['jam'] = $this->Admin_model->getAllDashboard(3);
			$data['hasil'] = $this->Admin_model->getAllhasil();
			$data['tahun'] = $this->Admin_model->getTahun();
			//$data['pemilih'] = $this->Admin_model->getPemilih();
			$selek = $this->Admin_model->get_ttpProdi();
			$fakultas = $selek[0]['fakultas'];
			$prodi = $selek[0]['prodi'];
			$data['pemilih'] = $this->Admin_model->getPemilih2($fakultas,$prodi);
			$data['fakultas'] = $this->Admin_model->getFakul();
			$data['ttpfak'] = $this->Admin_model->getTtpFakul();
			$hak = $data['hak'][0]["isi"];
			//get prodi apakah dia pemiluhan apa 
			$selek = $this->Admin_model->get_ttpProdi();
			$data['prodi'] = $selek[0]['prodi'];
			$data['kop'] = $this->Admin_model->get_Kop();

			if($prodi == 2){
				if($prodi == 2 && $fakultas == 1){
					$data['kop3'] = "UIN SUNAN AMPEL SURABAYA";
				}else {
					$data['kop3'] = $data['kop'][0]['fakultas'];
				}
			}else {
				$data['kop3'] = 'Prodi '.$data['kop'][0]['prodi'];
			}

			if($hak == 1){
				$admin = "hasil";
			}else {
				if($admin == 'hasil'){
					header("location:../../admin");
				}
			}
			if($simpan == "" || $simpan == null){
				$this->login();
			}else {
				$this->load->view('admin/header',$data);
				$this->load->view('admin/'.$admin,$data);
			}
		}
	}

	
	public function partai(){
		$data['jumlah'] = $this->Admin_model->getAllPartai();
		$data['setting'] = $this->Admin_model->setting();
		$data['calon'] = $this->Admin_model->getPartai();
		$selek = $this->Admin_model->get_ttpProdi();
		$data['prodi'] = $selek[0]['prodi'];
		
		$this->load->view('admin/header',$data);
		$this->load->view('admin/partai_home',$data);
		
	}
	public function jajal(){
		$this->load->view('login');
		
	}
	public function resets(){
		$selek = $this->Admin_model->get_ttpProdi();
		$fakultas = $selek[0]['fakultas'];
		$prodi = $selek[0]['prodi'];
		
		$data['mhs'] = $this->Admin_model->get_mhs($fakultas,$prodi);
		// print_r ($data['mhs']);
		$this->load->view('headerAbsen',$data);
		$this->load->view('absen2',$data);
		$this->load->view('footer',$data);
		
	}
	public function grafik(){
		$data['jk'] = $this->Admin_model->getJK();
		$data['hak'] = $this->Admin_model->getAllDashboard(2);
		$this->load->view('admin/header',$data);
		$this->load->view('grafik/grafik',$data);
		
	}
	public function batang($jk){
		$data['kel'] = $jk;
		if($jk == "1"){
			$jk = "L";
		}else {
			$jk = "P";
		}
		$selek = $this->Admin_model->get_ttpProdi();
		$data['prodi'] = $selek[0]['prodi'];
		
		$data['jk'] = $this->Admin_model->getJK();
		$data['hak'] = $this->Admin_model->getAllDashboard(2);
		$data['prodi'] = $this->Admin_model->getProdi($jk);
		$this->load->view('admin/header',$data);
		$this->load->view('grafik/batang',$data);
		
	}
	public function tabel($jk,$prodi){
		$prodi = str_replace("%20"," ",$prodi);
		$data['kel'] = $jk;
		if($jk == "1"){
			$jk = "L";
		}else {
			$jk = "P";
		}
		$selek = $this->Admin_model->get_ttpProdi();
		$data['prodi'] = $selek[0]['prodi'];
		
		$data['jk'] = $this->Admin_model->getJK();
		$data['hak'] = $this->Admin_model->getAllDashboard(2);
		$data['tabel'] = $this->Admin_model->getTable($jk,$prodi);
		//print_r ($data['tabel']);
		$this->load->view('admin/header',$data);
		$this->load->view('grafik/tabel',$data);
		
	}
	public function login(){
		$this->load->view('admin/login');	
	}
	public function tetapjam(){
		$jam = $this->input->post('jam');
		$this->Admin_model->tetapjam($jam);
		header("location:../admin/index/pengaturan");

	}
	public function tetapfak(){
		$fakultas = $this->input->post('fakultas');
		$prodi = $this->input->post('prodi');
		$this->Admin_model->tetapfak($fakultas,$prodi);
		//echo $prodi.$fakultas;
		header("location:../admin/index/pengaturan");
	}
	public function prodi($fak){
		$data['hasil'] = $this->Admin_model->getProdiFak($fak);
		//echo "<option value='1'>assss</option>";
	}
	public function hasil(){
		$data['hasil'] = $this->Admin_model->getAllhasil();
		echo $data['hasil'][0]['a'];
	}
	public function belum(){
		$selek = $this->Admin_model->get_ttpProdi();
		$fakultas = $selek[0]['fakultas'];
		$prodi = $selek[0]['prodi'];
		$data['pemilih'] = $this->Admin_model->getPemilih2($fakultas,$prodi);

		//$data['hasil'] = $this->Admin_model->getHasilMHS("SUM(`status` = '1') as belum");
		echo $data['pemilih'][0]['belum']." mhs";
	}
	public function sudah(){
		$selek = $this->Admin_model->get_ttpProdi();
		$fakultas = $selek[0]['fakultas'];
		$prodi = $selek[0]['prodi'];
		$data['pemilih'] = $this->Admin_model->getPemilih2($fakultas,$prodi);
		//$data['hasil'] = $this->Admin_model->getHasilMHS("SUM(`status` = '2') as sudah");
		echo $data['pemilih'][0]['sudah']." mhs";
	}
	public function pers_belum(){
		$selek = $this->Admin_model->get_ttpProdi();
		$fakultas = $selek[0]['fakultas'];
		$prodi = $selek[0]['prodi'];
		$data['pemilih'] = $this->Admin_model->getPemilih2($fakultas,$prodi);
		
		//$data['hasil'] = $this->Admin_model->getPemilih();
		//echo $data['hasil'][0]['sudah'];
		$hasil = $data['pemilih'][0]['belum'] / $data['pemilih'][0]['total'] ;
		$persen = $hasil * 100;
		echo number_format($persen,2)."% Belum Memilih";
	}
	public function pers_sudah(){
		$selek = $this->Admin_model->get_ttpProdi();
		$fakultas = $selek[0]['fakultas'];
		$prodi = $selek[0]['prodi'];
		$data['pemilih'] = $this->Admin_model->getPemilih2($fakultas,$prodi);
		
//		$data['hasil'] = $this->Admin_model->getPemilih();
		//echo $data['hasil'][0]['sudah'];
		$hasil = $data['pemilih'][0]['sudah'] / $data['pemilih'][0]['total'] ;
		$persen = $hasil * 100;
		echo number_format($persen,2)."% Sudah Absen";
	}
	public function cek(){
		$username = $this->input->post('username');
		$password = $this->input->post('password');
		
		$cek = $this->Admin_model->login($username,$password);
 		if($cek == 0){
			header("location:../admin");
		} else {
			$this->session->set_userdata('username',$username);
			header("location:../admin");

			
		}

	}
	public function hapus_all(){
		$ask = $this->input->post('ask');
		$cek = $this->Admin_model->login('hapus',$ask);
		if($cek == 0){
			header("location:../admin");
		} else {
			$this->Admin_model->all_hapus('hapus',$ask);
			$this->Admin_model->edit_konfirmasi('0');
			header("location:../admin");	
		}
	}
	public function ubah_judul(){
		$judul = $this->input->post('content');
		$this->Admin_model->edit_judul($judul);
		header("location:../admin/index/dashboard");
	}
	public function konfirmasi(){
		$this->Admin_model->edit_konfirmasi('1');
		header("location:../admin/index/hasil"); 
	}
	public function redirect(){
		$this->Admin_model->edit_konfirmasi('0');
		header("location:../admin/"); 
	}
	public function logout(){
		$this->session->unset_userdata(array('username' => ''));
		header("location:../admin");
	}
	public function tambah($id){
		$nim = $this->session->userdata('nim');
		$sex = $this->session->userdata('sex');
		$prodi = $this->session->userdata('prodi');
//		echo $nim." and ".$sex." and ".$prodi;	 
		$data = $this->Admin_model->duplicateAbsen($nim);
		$dupli = $nim."|".$id;
		//$duplicade = base64_encode($dupli);
		$duplicade = md5($dupli);
		if($data[0]['hasil'] <= 0){
			$this->Admin_model->tambah($id, $nim , $sex , $prodi,$duplicade);	
			$this->Admin_model->tambah2(md5($id), md5($nim) , md5($sex) , md5($prodi),$duplicade);	
		}
		$selek = $this->Admin_model->get_ttpProdi();
		$cek = $selek[0]['prodi'];
		
		//$cek = $this->Admin_model->setting();
		if($cek == 2){
			header("location:../../home/partai");
		}else {
			$this->session->unset_userdata(array('login' => ''));
			$this->session->unset_userdata(array('validasi' => ''));
			$data = $this->Admin_model->duplicateAbsen($nim);
			header("location:../../loading");			
		}
		
	}
	public function tambah_Pempartai($id){
		$nim = $this->session->userdata('nim');
		$sex = $this->session->userdata('sex');
		$prodi = $this->session->userdata('prodi');
//		echo $nim." and ".$sex." and ".$prodi;	 
		$data = $this->Admin_model->duplicateAbsenPar($nim);
		if($data[0]['hasil'] <= 0){
			$this->Admin_model->tambah_partai($id, $nim , $sex , $prodi);	
		}
		$this->session->unset_userdata(array('login' => ''));
		$this->session->unset_userdata(array('validasi' => ''));
		$data = $this->Admin_model->duplicateAbsen($nim);
		header("location:../../loading");
		
	}
	
	function hapusError(){
		$nim = $this->input->post('nimError');
		foreach($nim as $Hnim){
		   $this->Admin_model->nimError($Hnim);
		}
		header("location:../admin");
	}
	
	function tambah_calon(){
			$config['upload_path'] = './asset/foto/';
		   $config['allowed_types'] = 'jpg';
		   $config['max_size'] = '3000';
		   $config['max_width'] = '2024';
		   $config['max_height'] = '2024';
		   $config['file_name'] = strtolower('1');
		   
		   $this->load->library('upload',$config);
		   $upload = $this->upload->do_upload('userfile');
		    if(!$upload){
				$filepath = "default.jpg";
			}else{
				$data = array('upload_data'=>$this->upload->data());
				$filepath = $data['upload_data']['file_name'];
			}
		   $cek = $this->Admin_model->simpan_calon($filepath);
			//print_r ($filepath);
			header("location:../admin");
	}
	function tambah_partai(){
			$config['upload_path'] = './asset/foto/';
		   $config['allowed_types'] = 'jpg';
		   $config['max_size'] = '3000';
		   $config['max_width'] = '2024';
		   $config['max_height'] = '2024';
		   $config['file_name'] = strtolower('1');
		   
		   $this->load->library('upload',$config);
		   $upload = $this->upload->do_upload('userfile');
		    if(!$upload){
				$filepath = "default.jpg";
			}else{
				$data = array('upload_data'=>$this->upload->data());
				$filepath = $data['upload_data']['file_name'];
			}
		   $cek = $this->Admin_model->simpan_partai($filepath);
			//print_r ($filepath);
			header("location:../admin/partai");
	}
	function edit_calon($id){
		   $config['upload_path'] = './asset/foto/';
		   $config['allowed_types'] = 'jpg';
		   $config['max_size'] = '3000';
		   $config['max_width'] = '2024';
		   $config['max_height'] = '2024';
		   $config['file_name'] = strtolower('1');
		   
		   $this->load->library('upload',$config);
		   $upload = $this->upload->do_upload('userfile');
		    if(!$upload){
				$filepath = "default.jpg";
			}else{
				$data = array('upload_data'=>$this->upload->data());
				$filepath = $data['upload_data']['file_name'];
			}
			
		    $cek = $this->Admin_model->update_calon($filepath,$id);
			//print_r ($filepath);
			header("location:../admin");
	}
	function edit_partai($id){
		   $config['upload_path'] = './asset/foto/';
		   $config['allowed_types'] = 'jpg';
		   $config['max_size'] = '3000';
		   $config['max_width'] = '2024';
		   $config['max_height'] = '2024';
		   $config['file_name'] = strtolower('1');
		   
		   $this->load->library('upload',$config);
		   $upload = $this->upload->do_upload('userfile');
		    if(!$upload){
				$filepath = "default.jpg";
			}else{
				$data = array('upload_data'=>$this->upload->data());
				$filepath = $data['upload_data']['file_name'];
			}
			
		    $cek = $this->Admin_model->update_partai($filepath,$id);
			//print_r ($filepath);
			header("location:../../admin/partai");
	}
	function printAbsen(){
		$data['pemilih'] = $this->Admin_model->getPemilihAbsen();
		$this->load->view('admin/printAbsen',$data);
	}
	
}
