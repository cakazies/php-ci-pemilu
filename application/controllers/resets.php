	<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Resets extends CI_Controller {
	function __construct(){
		parent::__construct();
		$this->load->database();
		$this->load->library('session');
		$this->load->model('Admin_model');
		$this->load->helper(array('form','url','file','download','text'));
		error_reporting(E_ALL ^ (E_NOTICE | E_WARNING));

	}
	
	public function index(){
		$selek = $this->Admin_model->get_ttpProdi();
		$fakultas = $selek[0]['fakultas'];
		$prodi = $selek[0]['prodi'];
		$data['mhs'] = $this->Admin_model->get_mhs($fakultas,$prodi);
		$data['kop'] = $this->Admin_model->get_Kop();
		
		if($prodi == 2){
			$data['kop2'] = "DEWAN EKSEKUTIF MAHASISWA";
			if($prodi == 2 && $fakultas == 1){
				$data['kop3'] = "UIN SUNAN AMPEL SURABAYA";
			}else {
				$data['kop3'] = $data['kop'][0]['fakultas'];
			}
		}else {
			$data['kop2'] = "HIMPUNAN MAHASISWA";
			$data['kop3'] = 'Program Studi '.$data['kop'][0]['prodi'];
		}
		$this->load->view('headerAbsen',$data);
		$this->load->view('absen2',$data);
		$this->load->view('footer',$data);
	}
	public function ubah($nim){
		$data['password'] = md5('uinsa');
		
		$data['kop'] = $this->Admin_model->update_password_mhs($nim,$data);
		
		header("location:../");
		
 	}
}
