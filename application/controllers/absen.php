	<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Absen extends CI_Controller {
	function __construct(){
		parent::__construct();
		$this->load->database();
		$this->load->library('session');
		$this->load->model('Admin_model');
		$this->load->helper(array('form','url','file','download','text'));
		error_reporting(E_ALL ^ (E_NOTICE | E_WARNING));

	}
	
	public function getidjum(){
		$jum_pem = $this->Admin_model->getidjum();
		return json_encode ($jum_pem);
	}
	public function index(){
		$jum_pem = $this->Admin_model->getJum();
		$cekdup = 0;
		$i=0;
		$dataMHS ;
		foreach($jum_pem as $hjum_pem){		
			$dupli = $hjum_pem['nim']."|".$hjum_pem['id_calon'];
			//$duplicade = base64_encode($dupli);
			$duplicade = md5($dupli);
			if($duplicade == $hjum_pem['very']){
			}else {
				$dataMHS[$i] = $hjum_pem['nim'];
				$cekdup = 1;
				$i++;
			}
		}
		if($cekdup == 1){
			//echo "gagal Mas ";
			//print_r ($dataMHS);
			$data['error'] = $dataMHS;
			$data['syarat'] = '2';
			$this->load->view('admin/eror',$data);
		}else {
			$hak = $this->Admin_model->getAllDashboard(2);
			$cekHak = $hak[0]['isi'];
			if($cekHak == '0'){
				echo '<script> alert("KPU Belum Mengunci Calon Silahkan Hubungi KPU !");</script>';
			}else {			
				$selek = $this->Admin_model->get_ttpProdi();
				$fakultas = $selek[0]['fakultas'];
				$prodi = $selek[0]['prodi'];
				$data['mhs'] = $this->Admin_model->get_mhs($fakultas,$prodi);
				$data['kop'] = $this->Admin_model->get_Kop();
				
				if($prodi == 2){
					$data['kop2'] = "DEWAN EKSEKUTIF MAHASISWA";
					if($prodi == 2 && $fakultas == 1){
						$data['kop3'] = "UIN SUNAN AMPEL SURABAYA";
					}else {
						$data['kop3'] = $data['kop'][0]['fakultas'];
					}
				}else {
					$data['kop2'] = "HIMPUNAN MAHASISWA";
					$data['kop3'] = 'Program Studi '.$data['kop'][0]['prodi'];
				}
				$data['json'] = $this->getidjum();
				//print_r ($data['json']);
				$this->load->view('headerAbsen',$data);
				$this->load->view('absen',$data);
				$this->load->view('footer',$data);
			}
		}
	}
	public function ubah(){
		$nim = $this->input->post('nim');
		$password = $this->input->post('password');
		
		if($password == null || $password == ''){
			
		}else {
			$password = md5($this->input->post('password'));
		}
 		$data['cek'] = $this->Admin_model->cek_absen($nim,$password);	
		//print_r ($data['cek']);
		$hasil = $data['cek'][0]['hasil'];
		
		if($hasil == 0){
			$this->session->set_userdata('hasil','tidak');
			header("location:../absen");
		}else {
			$this->Admin_model->update_absen($nim,'2');	
			$this->session->set_userdata('hasil','ada');
			header("location:../absen");
		}
 	}
}
