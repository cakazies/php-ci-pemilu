	<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller {
	function __construct(){
		parent::__construct();
		$this->load->database();
		$this->load->library('session');		
		$this->load->model('Admin_model');
		$this->load->helper(array('form','url','file','download','text'));
		// $this->load->helper('menu_helper');
		error_reporting(E_ALL ^ (E_NOTICE | E_WARNING));
		
	}
	
	public function jajal(){
		$this->Admin_model->hapusDataMhs();
		$data['fakultas'] = $this->Admin_model->getNowFakultas();
		$this->load->view('print2',$data);

	}
	public function simpan(){
	
		$kode = $this->input->post('kode');
		$no = $this->input->post('no');
		$this->Admin_model->indertMHS($kode);
		echo $no;
	}
	public function index(){
		$jum_pem = $this->Admin_model->getJum();
		$cekdup = 0;
		$i=0;
		$dataMHS ;
		foreach($jum_pem as $hjum_pem){		
			$dupli = $hjum_pem['nim']."|".$hjum_pem['id_calon'];
			//$duplicade = base64_encode($dupli);
			$duplicade = md5($dupli);
			if($duplicade == $hjum_pem['very']){
			}else {
				$dataMHS[$i] = $hjum_pem['nim'];
				$cekdup = 1;
				$i++;
			}
		}
		if($cekdup == 1){
			$data['error'] = $dataMHS;
			$data['syarat'] = '2';
			$this->load->view('admin/eror',$data);
		}else {
			//$data = $this->Admin_model->getCalon();
			$hak = $this->Admin_model->getAllDashboard(2);
			$cekHak = $hak[0]['isi'];
			if($cekHak == '0'){
				echo '<script> alert("KPU Belum Mengunci Calon Silahkan Hubungi KPU !");</script>';
			}else {
				//$this->session->set_userdata('username',$username);
				$data['calon'] = $this->Admin_model->getCalon();
				$data['jumlah'] = $this->Admin_model->getAllCalon();
				$data['judul'] = $this->Admin_model->getAllDashboard();
				$data['kop'] = $this->Admin_model->get_Kop();
				$selek = $this->Admin_model->get_ttpProdi();
				$fakultas = $selek[0]['fakultas'];
				$prodi = $selek[0]['prodi'];

				if($prodi == 2){
					$data['kop2'] = "DEWAN EKSEKUTIF MAHASISWA";
					if($prodi == 2 && $fakultas == 1){
						$data['kop3'] = "UIN SUNAN AMPEL SURABAYA";
					}else {
						$data['kop3'] = $data['kop'][0]['fakultas'];
					}
				}else {
					$data['kop2'] = "HIMPUNAN MAHASISWA";
					$data['kop3'] = 'Program Studi '.$data['kop'][0]['prodi'];
				}
				$jum = $data['jumlah'][0]['a'];
				$simpan = $this->session->userdata('login');
				if($simpan != null || $simpan != ""){
					$this->load->view('header',$data);
					if($jum == 4){
						$this->load->view('form2',$data);
					}else if($jum == 3){
						$this->load->view('form1',$data);
					}else {
						$this->load->view('form',$data);
					} 
					$this->load->view('footer',$data);
					//header("location:".base_url()."index.php/login");
				}else {
					$this->load->view('login');
					//header("location:".base_url()."index.php/login");
				}
				
			}
		}
		
	}
	public function partai(){
		$hak = $this->Admin_model->getAllDashboard(2);
		$cekHak = $hak[0]['isi'];
		$url = base_url();
		if($cekHak == '0'){
			echo "<script> alert('KPU Belum Mengunci Calon Silahkan Hubungi KPU !');
			window.location.href = '".$url."';</script>";
		}else {
			$data['calon'] = $this->Admin_model->getPartai();
			$data['jumlah'] = $this->Admin_model->getAllPartai();
			$data['judul'] = $this->Admin_model->getAllDashboard();
			//print_r ($data['jumlah']);
			$selek = $this->Admin_model->get_ttpProdi();
			$fakultas = $selek[0]['fakultas'];
			$prodi = $selek[0]['prodi'];
			
			if($prodi == 2){
				$data['kop2'] = "DEWAN EKSEKUTIF MAHASISWA";
				if($prodi == 2 && $fakultas == 1){
					$data['kop3'] = "UIN SUNAN AMPEL SURABAYA";
				}else {
					$data['kop3'] = $data['kop'][0]['fakultas'];
				}
			}else {
				$data['kop2'] = "GENBI JAWA TIMUR";
			}

			$jum = $data['jumlah'][0]['a'];
			$simpan = $this->session->userdata('login');
			if($simpan != null || $simpan != ""){
				$this->load->view('partai_header',$data);
				if($jum == 4){
					$this->load->view('form2',$data);
				}else if($jum == 3){
					$this->load->view('form1',$data);
				}else {
					$this->load->view('partai_form',$data);
				} 
				$this->load->view('footer',$data);

			}else {
				//$this->load->view('login');
				header("location:".base_url()."index.php/login");
			}
		}
	}
}
