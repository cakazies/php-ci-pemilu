	<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard extends CI_Controller {
	function __construct(){
		parent::__construct();
		$this->load->database();
		$this->load->model('dashboard_model');
		$this->load->helper(array('form','url','file','download','text'));
		// $this->load->helper('menu_helper');
		error_reporting(E_ALL ^ (E_NOTICE | E_WARNING));
		
	}

}
